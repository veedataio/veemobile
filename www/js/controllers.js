'use strict';


var controllers = angular.module('recess.controllers', []);

controllers.config(function() {
  // configuration
});

controllers.controller('ActivityController', function($ionicPlatform, $rootScope, $scope, $moment, Goal, Health, Collector, ConnectivityMonitor) {

  ConnectivityMonitor.startWatching();
  // default initialization for variables used in the Activity View
  $scope.goals = {
    steps: 10000,
    active: 120,
  };

  $scope.data = {
    today: {
      steps: 0,
      active: [],
      activeTotal: 0,
    },
    week: {
      steps: [],
      active: [],
      activeTotal: 0,
      days: [],
    }
  };

  $scope.reached = {
    steps: false,
    active: false
  };


  for (var i = 6; i >= 0; i--) {
    if (i == 0) {
      $scope.data.week.days.push({
        label: 'Today',
        reached: {
          steps: false,
          active: false
        }
      });
    } else {
      $scope.data.week.days.push({
        label: $moment.utc().subtract(i, 'days').format('dd'),
        reached: {
          steps: false,
          active: false
        }
      });
    }
  }

  var refreshData = function() {
    var goals = Goal.getGoals();
    $scope.goals.steps = goals.steps;
    $scope.goals.active = goals.active;

    Health.getStepsToday(function(steps) {
      if (steps === undefined) {
        $scope.data.today.steps = 0;
      } else {
        $scope.data.today.steps = steps;
      }

    });

    Health.getStepsWeek(function(steps) {
      console.log('steps:');
      console.log(steps);
      if (steps.length > 0) {
        _.each(steps, function(step, idx) {
          var stepValue = Math.abs(step.value);
          $scope.data.week.steps[idx] = stepValue;
          if (stepValue >= $scope.goals.steps && $scope.goals.steps > 0) {
            $scope.data.week.days[idx].reached.steps = true;
          } else {
            $scope.data.week.days[idx].reached.steps = false;
          }
        });
      }
    });

    Health.getActiveToday(function(active) {
      console.log('Active data returned:');
      console.log(active);
      if (active !== undefined) {
        $scope.data.today.active = active.activity.min;
        $scope.data.today.activeTotal = active.total.min;
      } else {
        $scope.data.today.active = 0;
        $scope.data.today.activeTotal = 0;
      }

      $scope.reached.active = ($scope.data.today.activeTotal >= $scope.goals.active && $scope.data.today.activeTotal > 0);
      $scope.reached.steps = ($scope.data.today.steps >= $scope.goals.steps && $scope.data.today.steps > 0);

    });

    Health.getActiveWeek(function(active) {
      console.log('Active data of week returned:');
      console.log(active);
      if (active !== undefined) {
        active.days = _.sortKeysBy(active.days);
        var days = [];
        var total = 0;
        var idx = 0;
        _.each(active.days, function(day) {
          days.push(day.total.min);
          if (day.total.min >= $scope.goals.active && $scope.goals.active > 0) {
            $scope.data.week.days[idx].reached.active = true;
          } else {
            $scope.data.week.days[idx].reached.active = false;
          }
          total += day.total.min;
          idx += 1;
        });

        $scope.data.week.active = days;
        $scope.data.week.activeTotal = total;
      } else {
        $scope.data.week.active = 0;
        $scope.data.week.activeTotal = 0;
      }

    });



  };

  $ionicPlatform.ready(function() {
    console.log('ionic platform is ready');
    refreshData();

    $rootScope.$on('notification-goals-updated', function(event, goals) {
      console.log('Goals have been updated');
      $scope.goals.steps = goals.steps;
      $scope.goals.active = goals.active;
    });

    $scope.$on("$ionicView.enter", function(event, data) {
      console.log('entered activity controller');
      refreshData();
    });

  });


});

controllers.controller('ActivityController2', function($ionicPlatform, $rootScope, $scope, $moment, Health, Goal, DB, Collector) {

  _.mixin({
    'sortKeysBy': function(obj, comparator) {
      var keys = _.sortBy(_.keys(obj), function(key) {
        return comparator ? comparator(obj[key], key) : key;
      });

      return _.object(keys, _.map(keys, function(key) {
        return obj[key];
      }));
    }
  });

  $scope.goals = {
    steps: 0,
    active: 0,
  };

  $scope.reached = {
    steps: false,
    active: false
  };

  var refreshData = function() {
    var goals = Goal.getGoals();
    $scope.goals.steps = goals.steps;
    $scope.goals.active = goals.active;

    Health.getStepsToday(function(steps) {
      if (steps === undefined) {
        $scope.data.today.steps = 0;
      } else {
        $scope.data.today.steps = steps;
      }

    });

    Health.getStepsWeek(function(steps) {
      console.log('steps:');
      console.log(steps);
      if (steps.length > 0) {
        _.each(steps, function(step, i) {
          $scope.data.week.steps[i] = Math.abs(step.value);
        });
      }
    });

    Health.getActiveToday(function(active) {
      console.log('Active data returned:');
      console.log(active);
      if (active !== undefined) {
        $scope.data.today.active = active.activity.min;
        $scope.data.today.activeTotal = active.total.min;
      } else {
        $scope.data.today.active = 0;
        $scope.data.today.activeTotal = 0;
      }

    });

    Health.getActiveWeek(function(active) {
      console.log('Active data of week returned:');
      console.log(active);
      if (active !== undefined) {
        active.days = _.sortKeysBy(active.days);
        var days = [];
        var total = 0;
        var idx = 0;
        _.each(active.days, function(day) {
          days.push(day.total.min);
          if (day.total.min >= $scope.goals.active && $scope.goals.active > 0) {
            $scope.data.week.days[idx].reached.active = true;
          } else {
            $scope.data.week.days[idx].reached.active = false;
          }
          total += day.total.min;
          idx += 1;
        });

        $scope.data.week.active = days;
        $scope.data.week.activeTotal = total;
      } else {
        $scope.data.week.active = 0;
        $scope.data.week.activeTotal = 0;
      }

    });

    $scope.reached.active = ($scope.data.today.activeTotal >= $scope.goals.active && $scope.data.today.activeTotal > 0);

    $scope.reached.steps = ($scope.data.today.steps >= $scope.goals.steps && $scope.data.today.steps > 0);

  };


  $scope.week = [];

  // default initialization
  $scope.data = {
    today: {
      steps: 0,
      active: [],
      activeTotal: 0,
    },
    week: {
      steps: [],
      active: [],
      activeTotal: 0,
      days: [],
    }
  };

  for (var i = 6; i >= 0; i--) {
    if (i == 0) {
      $scope.data.week.days.push({
        label: 'Today',
        reached: {
          steps: false,
          active: false
        }
      });
    } else {
      $scope.data.week.days.push({
        label: $moment.utc().subtract(i, 'days').format('dd'),
        reached: {
          steps: false,
          active: false
        }
      });
    }
  }


  $ionicPlatform.ready(function() {
    console.log('ionic platform is ready');
    DB.init();
    refreshData();
    $scope.$on("$ionicView.enter", function(event, data) {
      refreshData();
    });


    $rootScope.$on('notification-goals-updated', function(event, goals) {
      console.log('Goals have been updated');
      $scope.goals.steps = goals.steps;
      $scope.goals.active = goals.active;
    });
  });


});

controllers.controller('MessengerController', function($ionicPlatform, $scope) {
  $scope.$on("$ionicView.enter", function(event, data) {
    console.log('entered messenger controller');
  });
});

controllers.controller('GoalController', function($rootScope, $scope, $ionicPopup, Goal, Configurator, $moment, $firebaseArray) {
  var goals = Goal.getGoals();
  $scope.goals = {
    steps: 0,
    active: 0,
  };


  $scope.$on("$ionicView.enter", function(event, data) {
    console.log('enter goal view');
    var goals = Goal.getGoals();
    $scope.goals.steps = goals.steps;
    $scope.goals.active = goals.active;


  });

  /*
   $rootScope.$on('notification-goals-updated', function(event, goals) {
   console.log('Goals have been updated');
   console.log(goals);
   $scope.goals.steps = goals.steps;
   $scope.goals.active = goals.active;
   });
   */

  $scope.setGoalSteps = function() {

    var popup = $ionicPopup.show({
      templateUrl: 'templates/ui-popup-goal-steps.html',
      title: 'Set your goal',
      subTitle: 'How many steps a day can you walk?',
      scope: $scope,
      cssClass: 'popup-goal-steps',
      buttons: [
        {
          text: 'Cancel',
          type: 'button-recess-cancel',

        },
        {
          text: '<b>Save</b>',
          type: 'button-recesss-steps',
          onTap: function(e) {
            return $scope.goals.steps;
          }
        }
      ]
    });

    popup.then(function(res) {
      console.log('Tapped!', res);
      if (res !== undefined) {
        $scope.goals.steps = res;
      }

    });

  };

  $scope.setGoalActive = function() {

    var popup = $ionicPopup.show({
      templateUrl: 'templates/ui-popup-goal-active.html',
      title: 'Set your goal',
      subTitle: 'How many minutes can you be active a day?',
      scope: $scope,
      cssClass: 'popup-goal-active',
      buttons: [
        {
          text: 'Cancel',
          type: 'button-recess-cancel',

        },
        {
          text: '<b>Save</b>',
          type: 'button-recesss-active',
          onTap: function(e) {
            return $scope.goals.active;
          }
        }
      ]
    });

    popup.then(function(res) {
      console.log('Tapped!', res);
      if (res !== undefined) {
        $scope.goals.active = res;
      }

    });

  };

  $scope.setReadyGo = function() {

    var popup = $ionicPopup.show({
      templateUrl: 'templates/ui-popup-goal-ready.html',
      title: 'Time to commit',
      subTitle: 'Your goals will be set and you will not be able to change them for 7 days.',
      cssClass: 'popup-ready',
      scope: $scope,
      buttons: [
        {
          text: 'Cancel',
          type: 'button-recesss-ready-cancel'
        },
        {
          text: '<b>Save</b>',
          type: 'button-recesss-ready',
          onTap: function(e) {
            return true;
          }
        }
      ]
    });

    popup.then(function(res) {
      console.log('Tapped!', res);
      if (res) {
        console.log('time to set the goals for good: ' + $scope.goals.steps);
        Goal.setGoal('steps', $scope.goals.steps);
        Goal.setGoal('active', $scope.goals.active);

        var ref = firebase.database().ref().child('users/' + $rootScope.user.uid + '/goals');
        $scope.firegoals = $firebaseArray(ref);

        $scope.firegoals.$add({
          timestamp: $moment().toJSON(),
          goals: $scope.goals
        });
      }

    });

  }
});

controllers.controller('DemoController', function($firebaseArray, _, $scope, $rootScope, Collector, Configurator, DB, $moment) {

  $scope.$on("$ionicView.enter", function(event, data) {
    if ($rootScope.user.uid !== 'N/A') {
      $scope.ref = firebase.database().ref().child('users/' + $rootScope.user.uid + '/steps');
      $scope.fireSteps = $firebaseArray($scope.ref);

    }
  });


  $scope.collect = function() {
    console.log('collector start');
    var ref = firebase.database().ref().child('users/' + $scope.user.uid + '/steps');
    $scope.fireSteps = $firebaseArray(ref);
    Collector.start();

  };

  $scope.viewSteps = function() {
    $scope.days = [];
    $scope.numberofdocumentsindb = 0;
    $scope.numberofsynced = 0;
    $scope.numberofunsynced = 0;

    DB.getAll('steps', 5000).success(function(data) {

      var days = _.groupBy(data, function(o) {
        return o.day;
      });

      _.each(days, function(day) {
        _.each(day, function(step) {
          console.log(step);
          $scope.numberofdocumentsindb += 1;
          if (step.synced) {
            $scope.numberofsynced += 1;
          } else {
            $scope.numberofunsynced += 1;
          }
        });
        $scope.days.push({
          showData: false,
          data: day,
          date: day[0].day,
        })

      })
    }).error(function(error) {
      console.error(error);
    });
  };

  $scope.clear = function() {
    DB.clear();
    Configurator.reset();
  };
});

controllers.controller('WelcomeController', function($scope, $state, $ionicSlideBoxDelegate, $ionicPopup, $timeout) {

  $ionicSlideBoxDelegate.enableSlide(false);
  $scope.permissions = {
    notifications: false,
    fitness: false,
  };

  // attempt to read from local storage if available if welcome screen prompted again
  var permissions = JSON.parse(window.localStorage.getItem('recess.permissions'));
  if (permissions !== undefined) {
    $scope.permission = permissions;
  }

  $scope.slideChanged = function(idx) {
    console.log('slide changed at ' + idx);
    var isSlideEnabled = true;
    if (idx == 1) {
      isSlideEnabled = false;
    }
    $timeout(function() {
      $ionicSlideBoxDelegate.enableSlide(isSlideEnabled);
    }, 0);
  };


  $scope.next = function(idx) {
    if (idx == 1 && !$scope.permissions.fitness) {
      promptRequirements();
    } else {
      $ionicSlideBoxDelegate.next();
    }

  };

  $scope.askPermissionHealth = function() {
    console.log('checking for plugin Health:');

    // disable sliding just in case
    $ionicSlideBoxDelegate.enableSlide(false);
    if (navigator.health) {
      console.log('Health available');
      navigator.health.isAvailable(function(success) {

        var datatypes = ['steps', 'distance', 'calories', 'calories.active', 'calories.basal',
          'activity', 'heart_rate'];

        navigator.health.requestAuthorization(datatypes,
          function(authSuccess) {
            console.log('requestAuth for health: ' + authSuccess);
            var permissions = {
              notifications: $scope.permissions.notifications,
              fitness: true,
            };
            window.localStorage.setItem('recess.permissions', JSON.stringify(permissions));
            // enable sliding again
            $ionicSlideBoxDelegate.enableSlide(true);
          }, function(authError) {
            console.log(authError);
          });

      }, function(error) {
        console.log(error);
        console.log('health not available');
        promptRequirements();
      });
    } else {
      console.log('health is not available.');

    }

  };

  function promptRequirements() {
    var popup = $ionicPopup.show({
      templateUrl: 'templates/ui-popup-info-permissions.html',
      title: 'Ooooppps...',
      subTitle: 'As a minimum we require access to your <span>fitness</span> data',
      scope: $scope,
      cssClass: 'popup-goal-steps',
      buttons: [
        {
          text: '<b>Ok</b>',
          type: 'button-recesss-steps',
          onTap: function(e) {
            return '';
          }
        }
      ]
    });

    popup.then(function(res) {
      console.log('Tapped!', res);
    });
  }

  $scope.askPermissionPush = function() {
    console.log('checking for push notifications');
  };


});


controllers.controller('RecessController', function($scope, $state, $ionicSideMenuDelegate, Configurator) {
  $scope.toggleMenu = function() {
    $ionicSideMenuDelegate.toggleRight();
  };

  $scope.accessDataCollection = function() {
    $scope.toggleMenu();
    $state.go('demo');
  };

  $scope.accessIntro = function() {
    $scope.toggleMenu();
    $state.go('welcome');
  }


});
