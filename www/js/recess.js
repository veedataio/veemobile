'use strict';

angular.module('recess', [
  'ionic', 'ngCordova', 'recess.controllers', 'recess.services', 'recess.directives', 'chart.js', 'angular-momentjs']
)

  .run(function($ionicPlatform, $rootScope) {
    var config = {
      apiKey: "AIzaSyC0JpuRromJF8kIKbDywDZJoqWyHzMTevY",
      authDomain: "recess-e9bf5.firebaseapp.com",
      databaseURL: "https://recess-e9bf5.firebaseio.com",
      storageBucket: "recess-e9bf5.appspot.com",
      messagingSenderId: "1038070322630"
    };
    firebase.initializeApp(config);
    $ionicPlatform.ready(function() {


      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar) {
        //StatusBar.styleBlackTranslucent();
        StatusBar.styleDefault();
      }


    });

  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    // force tabs for android to be displayed at the bottom as well
    $ionicConfigProvider.tabs.position('bottom');

    $stateProvider
    // Welcome
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'templates/welcome.html',
        controller: 'WelcomeController',
      })

      // Activity
      .state('activity', {
        url: '/activity',
        templateUrl: 'templates/tab-activity.html',
        controller: 'ActivityController',
      })
      // Messenger
      .state('messenger', {
        url: '/messenger',
        templateUrl: 'templates/tab-messenger.html',
        controller: 'MessengerController',
      })
      // Goal
      .state('goal', {
        url: '/goal',
        templateUrl: 'templates/tab-goal.html',
        controller: 'GoalController',
      })
      // Demo
      .state('demo', {
        url: '/demo',
        templateUrl: 'templates/demo.html',
        controller: 'DemoController',
      })
    ;


    $urlRouterProvider.otherwise(function($injector, $location) {
      console.log('See if it is a first time runner.');
      var $state = $injector.get('$state');
      var isFirstTime = window.localStorage.getItem('recess.firstTime');
      var permissions = JSON.parse(window.localStorage.getItem('recess.permissions'));

      if (permissions !== undefined && permissions !== null && permissions.fitness) {
        $state.go('activity');
      } else {
        window.localStorage.setItem('recess.firstTime', true);
        var permissions = {
          notifications: false,
          fitness: false,
        };
        window.localStorage.setItem('recess.permissions', JSON.stringify(permissions));
        $state.go('welcome');
      }


    });


  })

;
