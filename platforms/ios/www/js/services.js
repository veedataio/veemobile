'use strict';

var localhost = 'http://192.168.0.101:8000';
var azure = 'http://veedapi.azurewebsites.net';
var url = localhost;
var version = '/0.0.2';

angular.module('recess.services', ['ngResource', 'firebase'])

  .factory('_', function($window) {
    // Helper mixin to sort by keys and not just attributes
    $window._.mixin({
      'sortKeysBy': function(obj, comparator) {
        var keys = _.sortBy(_.keys(obj), function(key) {
          return comparator ? comparator(obj[key], key) : key;
        });

        return _.object(keys, _.map(keys, function(key) {
          return obj[key];
        }));
      }
    });
    return $window._;
  })

  .factory('Device', function($resource) {
    return $resource(url + version + '/devices/new',
      {},
      {});
  })

  .service('uuid4', function() {
    /**! http://stackoverflow.com/a/2117523/377392 */
    var fmt = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
    this.generate = function() {
      return fmt.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    };
  })

  .service('Collector', function($rootScope, $timeout, DB, $moment, Health, Configurator, $firebaseArray) {
    var _self = this;
    _self.count = 0;
    _self.executeIn = 4000;
    _self.timeout;
    $rootScope.isCollecting = false;


    $rootScope.$on('notification-db-opened', function() {
      console.log('DB opened, time to init Collector');
      _self.start();
    });

    _self.start = function() {
      var _self = this;
      console.log('starting collector');
      if (_self.timeout !== undefined && !$rootScope.isCollecting) {
        console.log('Already collecting!');
      } else {
        this.collect();
      }

    };

    _self.collect = function() {
      console.log('collect');
      var start = $moment();
      var _self = this;

      _self.timeout = $timeout(function() {
        console.log('Timeout: ' + _self.count++);
        $rootScope.isCollecting = true;
        if ($rootScope.user.uid == 'N/A') {
          console.log('user not initialized yet, try it later');
          $rootScope.isCollecting = false;
          return;
        }
        //
        var stepsPath = 'users/' + $rootScope.user.uid + '/steps';
        console.log('Firebase path: ' + stepsPath);
        var ref = firebase.database().ref().child(stepsPath);
        var fireSteps = $firebaseArray(ref);

        Configurator.getConfiguration('lastDayCollected', function(lastDay) {
          if (lastDay === undefined) {
            Configurator.setConfiguration({
              name: 'lastDayCollected',
              value: $moment().subtract(14, 'days').toJSON()
            }, function() {
              console.log('Last day initialized');
              $rootScope.isCollecting = false;
              //_self.collect();
            });
          } else {
            console.log('start collecting, beginning from: ' + lastDay.value);
            Health.getSteps(lastDay.value, function(steps) {
              console.log(steps);
              _.each(steps, function(step) {

                var data = step;
                var stepObject = {
                  day: $moment(step.startDate).format('YYYY.MM.DD'),
                  duration: $moment(step.endDate).diff($moment(step.startDate)),
                  timestamp: $moment(step.startDate).toJSON(),
                  data: data,
                };

                DB.add('steps', stepObject);

                fireSteps.$add(stepObject);

              });


              var nextDay = $moment(lastDay.value).add(1, 'days').startOf('day');
              $rootScope.isCollecting = false;

              if (nextDay.isAfter(moment(), 'day')) {
                console.log('Collected until today, leave it for now.');

              } else {
                // set next day and once it's done, trigger collection again.
                Configurator.setConfiguration({
                  name: 'lastDayCollected',
                  value: nextDay.toJSON(),
                }, function() {
                  _self.collect();
                });
                // make it available via rootScope
                $rootScope.lastdaycollected = nextDay.format('LLL');
              }
            });
          }
        });

      }, _self.executeIn);
    };


  })

  .factory('Auth', function($firebaseAuth) {
    return $firebaseAuth();
  })

  .factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork, $window, Configurator) {

    return {
      isOnline: function() {
        if (ionic.Platform.isWebView()) {
          return $cordovaNetwork.isOnline();
        } else {
          return navigator.onLine;
        }
      },
      isOffline: function() {
        if (ionic.Platform.isWebView()) {
          return !$cordovaNetwork.isOnline();
        } else {
          return !navigator.onLine;
        }
      },
      startWatching: function() {
        console.log('adding event listeners to monitor online status')
        if (ionic.Platform.isWebView()) {

          $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
            console.log("went online");
          });

          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
            console.log("went offline");
          });

        }
        else {

          $window.addEventListener("online", function(e) {
            console.log("went online");
            Configurator.initUser();
          }, false);

          $window.addEventListener("offline", function(e) {
            console.log("went offline");
          }, false);
        }
      }
    }
  })

  .service('Configurator', function($rootScope, Device, DB, $moment, Auth, uuid4, $firebaseObject, $firebaseUtils) {
    var _self = this;

    /**
     * Initialize Variables of rootScope
     *
     */
    $rootScope.user = {
      uid: 'N/A',
    };

    // As soon as Configurator is being triggered (through dependency on RecessController), init DB to get started.
    _self.initDB = function() {
      console.log('First, initialize DB');
      DB.init();
    }();

    $rootScope.$on('notification-db-opened', function() {
      console.log('DB opened, time to init Configurator');
      _self.registerDevice();
      _self.initUser();
      initConfig();
    });

    _self.initUser = function() {
      console.log('Initializing User');

      // if there's no internet connection, don't even try
      if (!_self.isOnline()) {
        console.log('no internet connection, aborting init user.');
        return;
      }
      // first look in DB if user exists
      _self.getConfiguration('user', function(dbUser) {
        if (dbUser !== undefined) {
          // use credentials to sign-in into firebase
          Auth.$signInWithEmailAndPassword(dbUser.email, dbUser.password).then(function(authData) {
            console.log("Logged in as:", authData.uid);
            $rootScope.user = dbUser; // make user available

          }).catch(function(error) {
            // something went wrong
            if (error.code === 'auth/user-not-found') {
              console.error('User has been deleted.'); // most likely
              _self.deleteConfiguration('user', function() {
                _self.initUser(); // simply create a new user
              });
            } else {
              console.error("Authentication failed:", error);
            }

          });
        } else {
          // create a new fake user to work with firebase email authentication
          var email = uuid4.generate() + '@doesnot.exist';
          var password = 'mydirtylittlesecret-' + uuid4.generate();
          console.log(email);
          console.log(password);

          Auth.$createUserWithEmailAndPassword(email, password).then(function(firebaseUser) {
            console.log('User created with uid: ' + firebaseUser.uid);
            // persist credentials for re-use
            _self.setConfiguration({
              name: 'user',
              email: email,
              password: password,
              uid: firebaseUser.uid,
            }, function(stored) {
              // console.log('User persisted.');
            });
            $rootScope.user = firebaseUser;

          }).catch(function(error) {
            console.error(error);
          });
        }
      });
    };

    _self.registerDevice = function() {
      console.log('Register device.');
      //TOOD: Read out more device information
      $rootScope.device = ionic.Platform.device();
    };

    /**
     * Initialize configuration
     * If config doesn't exist yet, it is created with default value.
     * - lastDayCollected: today - 14 days
     *
     */
    var initConfig = function() {
      _self.getConfiguration('lastDayCollected', function(lastDay) {
        if (lastDay === undefined || lastDay === null) {
          _self.setConfiguration({
            name: 'lastDayCollected',
            value: $moment().subtract(14, 'days').toJSON()
          }, function() {
            console.log('Last day initialized.');
          });
        }
      })

    };

    _self.reset = function() {
      _self.setConfiguration({
        name: 'lastDayCollected',
        value: $moment().subtract(14, 'days').toJSON()
      }, function() {
        console.log('config has been updated');
      });
    };

    _self.getConfiguration = function(config, callback) {
      DB.get('config', config).success(function(configuration) {
        callback(configuration);
      }).error(function(error) {
        console.error(error);
      });
    };

    _self.setConfiguration = function(config, callback) {
      if ($rootScope.user.uid.length > 3) { // N/A


        var ref = firebase.database().ref('users/' + $rootScope.user.uid + '/config/lastdaycollected').push();
        var fireRef = ref.child(config.name);
        var fireConfig = $firebaseObject(ref);
        fireConfig.$value = {
          value: config.value,
          timestamp: $moment().toJSON(),
        }
        fireConfig.$save();
        //$firebaseUtils.updateRec(fireConfig, config.value);
      }

      DB.add('config', config).success(function(configuration) {
        callback(configuration);
      }).error(function(error) {
        console.error(error);
      });
    };

    _self.deleteConfiguration = function(config, callback) {
      DB.delete('config', config).success(function(configuration) {
        callback(configuration);
      }).error(function(error) {
        console.error(error);
      });
    };

    var states = {};
    states['unknown'] = 'Unknown connection';
    states['ethernet'] = 'Ethernet connection';
    states['wifi'] = 'WiFi connection';
    states['2g'] = 'Cell 2G connection';
    states['3g'] = 'Cell 3G connection';
    states['4g'] = 'Cell 4G connection';
    states['cellular'] = 'Cell generic connection';
    states['none'] = 'No network connection';

    _self.isOnline = function() {
      if (navigator.connection !== undefined) {
        var state = navigator.connection.type;
        console.log(state);
        return !(state === 'none' || state === 'unknown');
      }
      return true;

    };

    _self.isWifi = function() {
      if (navigator.connection !== undefined) {
        var state = navigator.connection.type;
        console.log(state);
        return (state === 'wifi' || state === 'unknown');
      }
      return false;
    };

  })

  .service('Goal', function($q, $rootScope, $moment, DB) {
    var goals = {
      steps: 10000,
      active: 90,
    };


    $rootScope.$on('notification-db-opened', function() {
      console.log('DB opened, time to init Goals');
      init();
    });

    var _getGoals = function() {
      console.log('get goals');
      return goals;
    };


    var _getGoal = function(goal, callback) {
      DB.get('goals', goal).success(function(data) {
        if (data === undefined) {
          callback(data);
        } else {
          callback(data.value);
          goals[goal] = data.value;
        }
      }).error(function(error) {
        console.error(error);
      });
    };

    var _setGoal = function(goal, value) {
      console.log('set goal: ' + goal + ' to: ' + value);
      var goalObj = {
        name: goal,
        value: value,
      };

      DB.add('goals', goalObj);
      goals[goal] = value;
      console.log('Goals now: ');
      console.log(goals);
      $rootScope.$emit('notification-goals-updated', goals);
    };

    var init = function() {
      console.log('Initialize Goals');

      _getGoal('steps', function(steps) {
        console.log('Goal steps set to: ' + steps);

        if (steps === undefined) {
          _setGoal('steps', 10000);
          goals.steps = 10000;
        } else {
          goals.steps = steps;
        }

        _getGoal('active', function(active) {
          console.log('Goal active set to: ' + active);
          if (active === undefined) {
            _setGoal('active', 120);
            goals.active = 120;
          } else {
            goals.active = active;
          }
          $rootScope.$emit('notification-goals-updated', goals);
        });
      });
    };

    return {
      getGoal: _getGoal,
      setGoal: _setGoal,
      getGoals: _getGoals,
    }
  })

  .service('Health', function($q, $moment) {
    // if the service is not available, don't even try
    var cancelService = function() {
      if (navigator.health === undefined) {
        return true;
      }
      return false;
    };

    var _getActiveToday = function(callback) {
      console.log('Get active today:');

      var startDate = $moment().startOf('day');
      var endDate = $moment().endOf('day');
      getData(startDate, endDate, 'steps').success(function(data) {
        if (data.length > 0) {
          console.log('data');
          console.log(data);

          calculateDayActivity(data, function(activityData) {
            callback(activityData);
          });
        } else {
          callback();
        }

      }).error(function(error) {
        console.error(error);
      });

    };

    var calculateDayActivity = function(day, callback) {

      var minimumSteps = 10;
      var minimumDurationInMs = 10 * 1000; // 10sec

      var activity = {};

      _.each(day, function(step, i) {
        var start = $moment(step.startDate);
        var end = $moment(step.endDate);
        var difference = end.diff(start);
        var stepcount = step.value;

        //console.log(difference + ': ' + stepcount);
        activity[start.format('H')] = activity[start.format('H')] === undefined ? difference : activity[start.format('H')] + difference;

      });

      var activeDataMs = [];
      var activeDataMin = [];
      var totalMs = 0;
      var totalMin = 0;
      for (var i = 0; i < 24; i++) {
        if (activity[i] !== undefined) {
          var ms = activity[i];
          var min = $moment.duration(ms).minutes();
          totalMs += ms;
          totalMin += min;

          activeDataMs.push(ms);
          activeDataMin.push(min);
        } else {
          activeDataMs.push(0);
          activeDataMin.push(0);
        }

      }

      callback({
        activity: {
          ms: activeDataMs,
          min: activeDataMin,
        },
        total: {
          ms: totalMs,
          min: totalMin
        }
      });
    };

    var _getActiveWeek = function(callback) {
      console.log('Get active week:');

      var activity = {
        days: {},
        total: {
          ms: 0,
          min: 0,
        }
      };

      var daysCollected = 7;

      for (var i = 6; i >= 0; i--) {
        var startDate = $moment().startOf('day').subtract(i, 'days');
        var endDate = $moment().endOf('day').subtract(i, 'days');

        getData(startDate, endDate, 'steps').success(function(data) {
          if (data.length > 0) {
            var day = $moment(data[0].startDate).format('DD.MM.YYYY');
            console.log('Data for day: ' + day);
            console.log(data);

            calculateDayActivity(data, function(activityData) {
              activity.days[day] = activityData;
              activity.total.ms += activityData.total.ms;
              activity.total.min += activityData.total.min;

              // when last Data is reached, return callback
              if (Object.keys(activity.days).length === daysCollected) {
                callback(activity);
              }
            });
          } else {
            daysCollected -= 1;
            // check if we're already ready to send the results or not
            if (Object.keys(activity.days).length === daysCollected) {
              callback(activity);
            }
          }

        }).error(function(error) {
          console.error(error);
        });
      }


      /*
       console.log(startDate);
       console.log(endDate);
       getData(startDate, endDate, 'steps').success(function(data) {
       console.log('data from activeWeek');
       console.log(data);

       var days = _.groupBy(data, function(step) {
       return $moment(step.startDate).format('DD.MM-HH:00');
       });
       console.log(days);
       callback(days);
       });
       */

    };

    var _getStepsToday = function(callback) {
      console.log('Get steps today:');

      var startDate = $moment().startOf('day').add(1, 'second');
      var endDate = $moment().endOf('day');
      getAggregatedData(startDate, endDate, 'steps', 'day').success(function(data) {
        if (data[0] === undefined) {
          callback();
        } else {
          callback(data[0].value);
        }

      });
    };

    var _getStepsWeek = function(callback) {
      console.log('Get steps week:');
      var startDate = $moment.utc().startOf('day').subtract(6, 'days');
      var endDate = $moment().endOf('day');
      console.log('StartDate: ' + startDate + ' endDate: ' + endDate);
      getAggregatedData(startDate, endDate, 'steps', 'day').success(function(data) {
        console.log('data of week:');
        console.log(data);
        if (data.length > 0) {
          callback(data);
        } else {
          callback();
        }

      }).error(function(error) {
        console.log(error);
      });
    };

    var _getSteps = function(day, callback) {
      console.log('Get Steps of day: ' + day);
      var startDate = $moment(day).startOf('day');
      var endDate = $moment(day).endOf('day');
      getData(startDate, endDate, 'steps').success(function(data) {
        if (data.length > 0) {
          callback(data);
        } else {
          callback();
        }

      }).error(function(error) {
        console.error('Error retrieving steps: ' + error);
      })
    };

    var getAggregatedData = function(startDate, endDate, dataType, bucket) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      if (cancelService()) {
        deferred.reject('Health is not available.');
      } else {

        console.log('start: ' + startDate.toDate());
        console.log('end: ' + endDate.toDate());

        // Query health for aggregated data
        navigator.health.queryAggregated({
          startDate: startDate.toDate(),
          endDate: endDate.toDate(),
          dataType: dataType,
          bucket: bucket,
        }, function(results) {
          console.log('Aggregated Data:');
          console.log(results);
          deferred.resolve(results);
        }, function(error) {
          console.error(error);
          deferred.reject(error);
        });
      }

      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };


    /**
     * Get data of a specific day
     *
     * @param day
     * @param dataType Type of data
     * @returns Array of steps
     */
    var getData = function(startDate, endDate, dataType) {
      var deferred = $q.defer();
      var promise = deferred.promise;

      if (cancelService()) {
        deferred.reject('Health is not available.');
      } else {
        console.log('Get health data:');
        console.log('startDate: ' + $moment(startDate).toDate());
        console.log('endDate: ' + $moment(endDate).toDate());
        console.log('dataType: ' + dataType);

        // Query health
        navigator.health.query({
          startDate: $moment(startDate).toDate(),
          endDate: $moment(endDate).toDate(),
          dataType: dataType,
        }, function(results) {
          deferred.resolve(results);
        }, function(error) {
          console.error(error);
          deferred.reject(error);
        });

      }

      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };

    return {
      getStepsToday: _getStepsToday,
      getStepsWeek: _getStepsWeek,
      getSteps: _getSteps,

      getActiveToday: _getActiveToday,
      getActiveWeek: _getActiveWeek,
    };
  })


  .factory('DB', function($rootScope, $window, $q, _) {
    var indexedDB = $window.indexedDB;
    var db = null;

    var _init = function() {
      var deferred = $q.defer();
      console.log('open DB');
      if (db !== null) {
        console.log('DB already opened.');
        deferred.resolve();
      }

      var version = 3;
      var request = indexedDB.open("recess", version);

      request.onupgradeneeded = function(e) {
        db = e.target.result;

        e.target.transaction.onerror = indexedDB.onerror;

        if (db.objectStoreNames.contains('steps')) {
          db.deleteObjectStore('steps');
        }
        if (db.objectStoreNames.contains('config')) {
          db.deleteObjectStore('config');
        }
        if (db.objectStoreNames.contains('goals')) {
          db.deleteObjectStore('goals');
        }

        var objectStoreSteps = db.createObjectStore('steps', {
          autoIncrement: true,
          keyPath: 'id'
        });
        objectStoreSteps.createIndex('day', 'day', {unique: false});
        objectStoreSteps.createIndex('timestamp', 'timestamp', {unique: false});
        objectStoreSteps.createIndex('source', 'source.sourceName', {unique: false});

        var objectStoreConfig = db.createObjectStore('config', {
          keyPath: 'name'
        });

        var objectStoreGoals = db.createObjectStore('goals', {
          keyPath: 'name',
        });
      };

      request.onsuccess = function(e) {
        db = e.target.result;
        console.log('DB opened.');
        $rootScope.$emit('notification-db-opened');
        deferred.resolve();
      };

      request.onerror = function() {
        console.log('DB opening rejected.');
        deferred.reject();
      };

      return deferred.promise;
    };


    var _clear = function(objectstores) {
      // open a read/write db transaction, ready for clearing the data
      var transaction = db.transaction(['steps'], 'readwrite');

      // report on the success of opening the transaction
      transaction.oncomplete = function(event) {
        console.log('Transaction completed: database modification finished.');
      };


      transaction.onerror = function(event) {
        console.log('Transaction not opened due to error: ' + transaction.error);
      };

      // if no store is defined, just clear steps
      if (objectstores === undefined) {
        objectstores = ['steps'];
      }

      _.each(objectstores, function(store) {
        // create an object store on the transaction
        var objectStore = transaction.objectStore(store);

        // clear all the data out of the object store
        var objectStoreRequest = objectStore.clear();

        objectStoreRequest.onsuccess = function(event) {
          // report the success of our clear operation
          console.log('cleared: ' + store);
        };
      });


    };
    var _getAll = function(table, amount) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      if (amount === undefined) {
        amount = 200;
      }
      var results = [];

      if (db === null) {
        deferred.reject("IndexDB is not opened yet!");
      } else {
        var transaction = db.transaction(table, 'readwrite');
        var objectStore = transaction.objectStore(table);

        var cursor = objectStore.openCursor();

        cursor.onsuccess = function(event) {
          var result = event.target.result;
          if (result === null || result === undefined) {
            deferred.resolve(results);
          } else {
            results.push(result.value);

            if (amount === results.length) {
              deferred.resolve(results);
            } else {
              result.continue();
            }
          }

        };

        cursor.onerror = function(event) {
          console.error(event.value);
          deferred.reject();
        };
      }

      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };

    var _get = function(table, key) {
      var deferred = $q.defer();
      var promise = deferred.promise;

      if (db === null) {
        deferred.reject("IndexDB is not opened yet!");
      } else {
        var transaction = db.transaction(table, 'readwrite');
        var objectStore = transaction.objectStore(table);

        var request = objectStore.get(key);

        request.onsuccess = function(event) {
          var result = request.result;
          deferred.resolve(result);
        };

        request.onerror = function(event) {
          console.error(event.value);
          deferred.reject();
        };
      }

      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };

    var _add = function(table, object) {
      var deferred = $q.defer();
      var promise = deferred.promise;

      var transaction = db.transaction(table, 'readwrite');
      var objectStore = transaction.objectStore(table);

      var request = objectStore.put(object);

      request.onsuccess = function(event) {
        var result = request.result;
        deferred.resolve(result);
      };

      request.onerror = function(event) {
        console.error(event.value);
        deferred.reject();
      };


      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };

    var _delete = function(table, object) {
      var deferred = $q.defer();
      var promise = deferred.promise;

      var transaction = db.transaction(table, 'readwrite');
      var objectStore = transaction.objectStore(table);

      var request = objectStore.delete(object);

      request.onsuccess = function(event) {
        var result = request.result;
        deferred.resolve(result);
      };

      request.onerror = function(event) {
        console.error(event.value);
        deferred.reject();
      };


      promise.success = function(fn) {
        promise.then(fn);
        return promise;
      };

      promise.error = function(fn) {
        promise.then(null, fn);
        return promise;
      };

      return promise;
    };

    return {
      init: _init,
      get: _get,
      getAll: _getAll,
      add: _add,
      clear: _clear,
      delete: _delete,
    };

  })


;
